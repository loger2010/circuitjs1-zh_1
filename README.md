# circuitjs1-zh

#### 介绍
功能强大的基于HTML5的电路模拟器，circuitjs1的中文版本。

#### 项目
该项目已合并至[https://github.com/pfalstad/circuitjs1](https://github.com/pfalstad/circuitjs1)，这里只提供原仓库编译后生成的文件，方便部署使用。